import 'package:flutter/material.dart';

import '../main.dart';

class Splash extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _SplashState();
  }

}

class _SplashState  extends State<Splash>{

  @override
  void initState() {
    super.initState();
    _navigateToHome();
  }

  _navigateToHome() async{
    await Future.delayed(Duration(milliseconds: 1500));
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MyHomePage()));
  }
      

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Image.asset('assets/splash.jpg', fit: BoxFit.fill),
      ),
    );
  }
}