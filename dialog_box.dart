import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DialogBox{
  static Widget dialog(
      {required BuildContext context,
        required Function()? onPressed,
        required Function()? onTap,
        required TextEditingController textEditingController1,
        required TextEditingController textEditingController2,
        required TextEditingController textEditingController3,
        required TextEditingController textEditingController4,
        required FocusNode input1FocusNode,
        required FocusNode input2FocusNode,
        required FocusNode input3FocusNode,
        required FocusNode input4FocusNode}
      ){
    return Container(
        height: MediaQuery.of(context).size.height ,
        width: MediaQuery.of(context).size.width -200,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextFormField(
              controller: textEditingController1,
              keyboardType: TextInputType.datetime,
              focusNode: input1FocusNode,
              decoration: InputDecoration(hintText: "Date",labelText: "Date",
                  icon: Icon(Icons.calendar_today)),
              autofocus: true,
              onTap: onTap,
              onFieldSubmitted: (value){
                input1FocusNode.unfocus();
                FocusScope.of(context).requestFocus(input2FocusNode);
              },
            ),
            TextFormField(
              controller: textEditingController2,
              keyboardType: TextInputType.text,
              focusNode: input2FocusNode,
              decoration: InputDecoration(hintText: "Name",icon: Icon(Icons.account_circle),labelText: "Name"),
              autofocus: true,
              onFieldSubmitted: (value){
                input2FocusNode.unfocus();
                FocusScope.of(context).requestFocus(input3FocusNode);
              },
            ),
            TextFormField(
              controller: textEditingController3,
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              inputFormatters: [FilteringTextInputFormatter.allow(RegExp(r'[0-9]+\.?[0-9]*'))],
              focusNode: input3FocusNode,
              decoration: InputDecoration(hintText: "Amount",icon: Icon(Icons.account_balance),labelText: "Amount"),
              autofocus: true,
              validator: (value) {
                if (value!.isEmpty)
                  return "Please enter amount for this transaction";
                return null;
              },
              onFieldSubmitted: (value){
                input3FocusNode.unfocus();
                FocusScope.of(context).requestFocus(input4FocusNode);

              },
            ),

            TextFormField(
              controller: textEditingController4,
              keyboardType: TextInputType.text,
              focusNode: input4FocusNode,
              decoration: InputDecoration(hintText: "Description"),
              onFieldSubmitted: (value){
                input4FocusNode.unfocus();
              },
            )
          ]

        )
        );

  }
}