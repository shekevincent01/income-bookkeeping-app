import 'package:intl/intl.dart';

class Item{
  int id;
  String name;
  String description;
  double amount;
  String category;
  DateTime dateEntered;
  String dateTransaction;
  String image;

  Item ( this.id, this.name, this.description, this.amount,this.category, this.dateEntered,
      this.dateTransaction,this.image);


  Map<String, dynamic> toMap() => {
    "id": id,
    "name": name,
    "description": description,
    "amount": amount,
    "category": category,
    "date_entered": dateEntered.toIso8601String(),
    "date_transaction": dateTransaction,
    "image": image
  };

  factory Item.fromMap(Map<String, dynamic> data) {
    return Item(
      data['id'] as int,
      data['name'] as String,
      data['description'] as String,
      data['amount'] as double,
      data['category'] as String,
      DateTime.parse(data['date_entered'] as String),
      data['date_transaction'] as String,
      data['image'] as String,
    );
  }
}