import 'package:eb2_app/products.dart';
import 'package:flutter/material.dart';

class ProductManager extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _ProductManagerState();
  }
  
}

class _ProductManagerState extends State<ProductManager>{

  List<String> product = ['Decor','Get sense'];

  @override
  Widget build(BuildContext context) {
    return ListView(children: [Container(
        margin: EdgeInsets.all(10.0),
        child: RaisedButton(
          onPressed: () {
            setState(() {
              //product.removeLast();
              product.add('get Lost');
            });
          },
          child: Text('Add Stuff'),
        )
    ),
      Products(product)
    ]
    );
  }
  
}