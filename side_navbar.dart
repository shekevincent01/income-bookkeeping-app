import 'package:eb2_app/services/auth.dart';
import 'package:flutter/material.dart';

class SideNavbar extends StatelessWidget {

  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[ SizedBox(height: 120.0,
          child: DrawerHeader(
            child: MaterialButton(
              shape: CircleBorder(),
              color: Colors.white,
              padding: EdgeInsets.all(20),
              onPressed: () { Navigator.of(context).pop();},
              child: Icon(
                Icons.close,
                size: 30,
                color: Colors.black,
              ),
            ),

          ),
        ),

          ListTile(
            leading: Icon(Icons.input),
            title: Text('Welcome'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.verified_user),
            title: Text('Profile'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text('Settings'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.border_color),
            title: Text('Feedback'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Logout'),
            onTap: () async => await _auth.signOut(),
          ),
        ],
      ),
    );
  }
}