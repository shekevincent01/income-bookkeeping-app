import 'dart:math';

import 'package:eb2_app/model/item.dart';
import 'package:eb2_app/widgets/dialog_box.dart';
import 'package:eb2_app/widgets/item_card_widget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../database.dart';

class Earnings extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _EarningState();
  }
}

class _EarningState extends State<Earnings> {

  final SQLiteDbProvider dbManager = SQLiteDbProvider.instance;

  List<Item>? itemList;

  //DateTime dateToday =DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
  DateTime dateToday =DateTime.now();

  TextEditingController input1 = TextEditingController();
  TextEditingController input2 = TextEditingController();
  TextEditingController input3 = TextEditingController();
  TextEditingController input4 = TextEditingController();
  late FocusNode input1FocusNode;
  late FocusNode input2FocusNode;
  late FocusNode input3FocusNode;
  late FocusNode input4FocusNode;

  DateTime selectedDate = DateTime.now();

  void initState() {
    super.initState();
    input1FocusNode = FocusNode();
    input2FocusNode = FocusNode();
    input3FocusNode = FocusNode();
    input4FocusNode = FocusNode();
  }

  @override
  void dispose() {
    input1FocusNode.dispose();
    input2FocusNode.dispose();
    input3FocusNode.dispose();
    input4FocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () { 
          showDialog(context: context,
              builder: (_) =>
          new AlertDialog(
            scrollable: true,
            title: Text("Enter Transaction Data"),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))
            ),
            content: DialogBox.dialog(
                context: context,
                onPressed: () {
                  Item item = Item(
                      Random().hashCode,
                      input2.text,
                      input4.text,
                      double.parse(input3.text),
                      'Earnings',
                      dateToday,
                      input1.text,
                      "assets/Screenshot (1).png");
                  dbManager.insert(item);

                  setState(() {
                    input1.text = "";
                  });
                  Navigator.of(context).pop();
                },
                onTap: ()async{
                  final DateTime? picked = await showDatePicker(
                    context: context,
                    initialDate: selectedDate,
                    firstDate: DateTime(2000),
                    lastDate: DateTime(2025),
                  );
                  if (picked != null && picked != selectedDate)
                    setState(() {
                      selectedDate = picked;
                      input1.text = DateFormat('yyyy/MM/dd').format(picked).toString();
                    });
                },
                textEditingController1: input1,
                textEditingController2: input2,
                textEditingController3: input3,
                textEditingController4: input4,
                input1FocusNode: input1FocusNode,
                input2FocusNode: input2FocusNode,
                input3FocusNode: input3FocusNode,
                input4FocusNode: input4FocusNode),
              actions: [
                MaterialButton(onPressed: (){
                  Navigator.of(context).pop();

                  setState(() {
                    input1.text = "";
                    input2.text = "";
                    input3.text = "";
                    input4.text = "";
                  });
                },
                  color: Colors.redAccent,
                  child: Text("Cancel"),
                ),
                MaterialButton(onPressed: () {
                  Item item = Item(
                      Random().hashCode,
                      input2.text,
                      input4.text,
                      double.parse(input3.text),
                      'Earnings',
                      dateToday,
                      input1.text,
                      "assets/Screenshot (1).png");
                  dbManager.insert(item);

                  setState(() {
                    setState(() {
                      input1.text = "";
                      input2.text = "";
                      input3.text = "";
                      input4.text = "";
                    });
                  });
                  Navigator.of(context).pop();
                },
                  color: Colors.blue,
                  child: Text("Save"),
                )
              ]
          ));}),
        body: FutureBuilder(
          future: dbManager.getItemsByCategory("Earnings"),
          //future: dbManager.getAllItems(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              itemList = snapshot.data;
              print(snapshot);
              return ListView.builder(
                itemCount: itemList!.length,
                itemBuilder: (BuildContext context, int index) {
                  Item _item = itemList![index];
                  return ItemCard(_item, () {
                    dbManager.delete(_item.id);
                    setState(() {});
                  }, () {
                    input1.text = _item.dateTransaction;
                    input2.text = _item.name;
                    input3.text = _item.amount.toString();
                    input4.text = _item.description;
                    showDialog(context: context,
                        builder: (_) =>
                        new AlertDialog(
                            scrollable: true,
                            title: Text("Enter Transaction Data"),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(10.0))
                            ),
                            content: DialogBox.dialog(
                                context: context,
                                onPressed: () {
                                  Item item = Item(
                                      _item.id,
                                      input2.text,
                                      input4.text,
                                      double.parse(input3.text),
                                      'Earnings',
                                      dateToday,
                                      input1.text,
                                      _item.image);
                                  dbManager.update(item);

                                  setState(() {
                                    input1.text = "";
                                    input2.text = "";
                                    input3.text = "";
                                    input4.text = "";
                                  });
                                  Navigator.of(context).pop();
                                },
                                onTap: ()async{
                                  final DateTime? picked = await showDatePicker(
                                    context: context,
                                    initialDate: selectedDate,
                                    firstDate: DateTime(2000),
                                    lastDate: DateTime(2025),
                                  );
                                  if (picked != null && picked != selectedDate)
                                    setState(() {
                                      selectedDate = picked;
                                      input1.text = DateFormat('yyyy/MM/dd').format(picked).toString();
                                    });
                                },
                                textEditingController1: input1,
                                textEditingController2: input2,
                                textEditingController3: input3,
                                textEditingController4: input4,
                                input1FocusNode: input1FocusNode,
                                input2FocusNode: input2FocusNode,
                                input3FocusNode: input3FocusNode,
                                input4FocusNode: input4FocusNode),
                            actions: [
                              MaterialButton(onPressed: (){
                                Navigator.of(context).pop();

                                setState(() {
                                  input1.text = "";
                                  input2.text = "";
                                  input3.text = "";
                                  input4.text = "";
                                });
                              },
                                color: Colors.redAccent,
                                child: Text("Cancel"),
                              ),
                              MaterialButton(onPressed: () {
                                Item item = Item(
                                    Random().hashCode,
                                    input2.text,
                                    input4.text,
                                    double.parse(input3.text),
                                    'Earnings',
                                    dateToday,
                                    input1.text,
                                    "assets/Screenshot (1).png");
                                dbManager.insert(item);

                                setState(() {
                                  setState(() {
                                    input1.text = "";
                                    input2.text = "";
                                    input3.text = "";
                                    input4.text = "";
                                  });
                                });
                                Navigator.of(context).pop();
                              },
                                color: Colors.blue,
                                child: Text("Save"),
                              )
                            ]
                        ));
                  });
                },
              );
            }

            return Column();
          },
        )
          );
  }}
