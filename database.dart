import 'dart:async';
import 'dart:io';
import 'package:eb2_app/model/item.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class SQLiteDbProvider {
  SQLiteDbProvider._();

  static final SQLiteDbProvider instance = SQLiteDbProvider._();
  static Database? _database;

  Future<Database> get database async {
    if (_database != null) {print(await _database!.rawQuery('SELECT * FROM item ORDER BY name;'));
    //print(await _database!.rawQuery('SELECT * FROM sqlite_master ORDER BY name;'));
      //await _database!.rawDelete('Delete from item ');
    //print(await getApplicationDocumentsDirectory());
    return _database!;}

    _database = await _initDB();
    return _database!;
  }

  Future<Database> _initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "ItemDB.db");

    return await openDatabase(
        path, version: 1,
        onOpen: (db) { },
        onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    await db.execute(
        "CREATE TABLE Item ("
            "id INTEGER PRIMARY KEY autoincrement,"
            "name TEXT,"
            "description TEXT,"
            "amount INTEGER,"
            "category TEXT,"
            "date_entered TEXT,"
            "date_transaction TEXT,"
            "image TEXT"")"
    );
    db.execute("INSERT INTO Item ('name', 'description', 'amount', 'category','date_entered','date_transaction','image' ) "
        "values (?,?,?,?)",
        ["iPhone","iPhone is the stylist phone ever",1000, "Gas","02-02-2021","02-02-2021","'assets/Screenshot (1).png'"]);
  }

  Future close() async {
    final db = await instance.database;

    db.close();
  }


  Future<List<Item>> getAllItems() async {
    final db = await instance.database;
    final results = await db.query(
        "Item", orderBy: "id ASC");
    return results.map((result) => Item.fromMap(result)).toList();
  }

  Future<List<Item>> getItemsByCategory(String category) async {
    final db = await instance.database;
    final results = await db.query("Item",where: "category=? ", whereArgs: [category] );
    return results.map((result) => Item.fromMap(result)).toList();
  }

  Future<Item?> getItemById(int id) async {
    final db = await instance.database;
    var result = await db.query("Item", where: "id =? ", whereArgs: [id]);
    return result.isNotEmpty ? Item.fromMap(result.first):null;
  }

  insert(Item item) async {
    final db = await instance.database;

    final id = await db.insert("Item", item.toMap());
    return id;
  }

  update(Item item) async {
    final db = await instance.database;
    var result = await db.update(
        "Item", item.toMap(), where: "id = ?", whereArgs: [item.id]
    );
    return result;
  }

  delete(int id) async {
    final db = await instance.database;
    db.delete("Item", where: "id = ?", whereArgs: [id]);
  }
}