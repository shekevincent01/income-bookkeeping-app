import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'dart:developer';

class BottomNavBar extends StatefulWidget{
  final Function changeBody;

  BottomNavBar(this.changeBody);

  @override
  State<StatefulWidget> createState() {
    return _NavState();
  }
}

class _NavState extends State<BottomNavBar>{
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: _selectedIndex, // this will be set when a new tab is tapped
      showSelectedLabels: true,
      showUnselectedLabels: true,
      items: [
        BottomNavigationBarItem(
          icon: new Icon(Icons.home),
          label: 'Home', backgroundColor: Colors.green,
            activeIcon: Icon(Icons.home, color: Colors.redAccent)
        ),
        BottomNavigationBarItem(
          icon: new Icon(Icons.mail),
          label: 'Transactions',
         backgroundColor: Colors.green,
          activeIcon: Icon(Icons.mail, color: Colors.redAccent,)
        ),
        BottomNavigationBarItem(
            icon: new Icon(Icons.mail),
            label: 'Earnings',
            backgroundColor: Colors.green,
            activeIcon: Icon(Icons.mail, color: Colors.redAccent,)
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Expenses',
            backgroundColor: Colors.green,
          activeIcon: Icon(Icons.person, color: Colors.redAccent,)
        )
      ],
      onTap: (index){

        setState(() {
            _selectedIndex=index;
            widget.changeBody(index);
        });

    },
    );
  }
}

