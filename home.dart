import 'package:eb2_app/product_manager.dart';
import 'package:eb2_app/screens/transactions.dart';
import 'package:eb2_app/widgets/bottom_navbar.dart';
import 'package:eb2_app/widgets/side_navbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'earnings.dart';
import 'expenses.dart';

class Home extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
      theme: ThemeData(primarySwatch: Colors.lightGreen,
        canvasColor: Colors.lightGreen.shade50,),
    );
  }
}

class HomePage extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<HomePage> {

  int _selectedIndex = 0;

  List<Widget> _screenOptions = <Widget>[
    ProductManager(),
    Transactions(),
    Earnings(),
    Expenses()
  ];

  void _changeBody(int index){
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      drawer: SideNavbar(),
      appBar: AppBar(
          title: Text('Taxified')
      ),
      body: _screenOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavBar(_changeBody),
    );
  }
}
