import 'package:eb2_app/model/item.dart';
import 'package:flutter/material.dart';

class ItemCard extends StatefulWidget{

  Item item;
  Function()? onDeletePress;
  Function()? onEditPress;

  ItemCard(this.item, this.onDeletePress, this.onEditPress);

  @override
  State<StatefulWidget> createState() {
    return _ItemCardState();
  }

}

class _ItemCardState extends State<ItemCard>{
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Card(
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '${widget.item.dateTransaction}',
                    style: TextStyle(fontSize: 15),
                  ),
                  Text(
                    '${widget.item.name}',
                    style: TextStyle(fontSize: 15),
                  ),
                  Text(
                    '${widget.item.category}',
                    style: TextStyle(fontSize: 15),
                  ),
                  Text( '\$${widget.item.amount}',
                    style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),
                  ),
                  Text(
                    '${widget.item.description}',
                    style: TextStyle(fontSize: 15),
                  )
                ],
              ),
              Row(
                children: [
                  CircleAvatar(
                    backgroundColor: Colors.white,
                    child: IconButton(
                      icon: Icon(
                        Icons.edit,
                        color: Colors.blueAccent,
                      ),
                      onPressed:  widget.onEditPress,
                    ),
                  ),
                  SizedBox(width:15),
                  CircleAvatar(
                    backgroundColor: Colors.white,
                    child: IconButton(
                      icon: Icon(
                        Icons.delete,
                        color: Colors.redAccent,
                      ),
                      onPressed: widget.onDeletePress,
                    )
                  )],
              )
            ],
          ),
        ),
      ));
  }

}