import 'package:eb2_app/authentication/register.dart';
import 'package:eb2_app/authentication/sign_in.dart';
import 'package:flutter/material.dart';

class Authenticate extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _AuthenticateState();
  }

}

class _AuthenticateState extends State<Authenticate>{
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SignIn(),
    );
  }
}