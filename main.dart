import 'package:eb2_app/model/member.dart';
import 'package:eb2_app/product_manager.dart';
import 'package:eb2_app/screens/earnings.dart';
import 'package:eb2_app/screens/expenses.dart';
import 'package:eb2_app/screens/splash.dart';
import 'package:eb2_app/screens/transactions.dart';
import 'package:eb2_app/screens/wrapper.dart';
import 'package:eb2_app/services/auth.dart';
import 'package:eb2_app/widgets/bottom_navbar.dart';
import 'package:eb2_app/widgets/side_navbar.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}


class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Splash(),
      theme: ThemeData(primarySwatch: Colors.lightGreen,
        canvasColor: Colors.lightGreen.shade50,),
    );
  }
}

class MyHomePage extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<MyHomePage> {

  int _selectedIndex = 0;

  List<Widget> _screenOptions = <Widget>[
    ProductManager(),
    Transactions(),
    Earnings(),
    Expenses()
  ];

  void _changeBody(int index){
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {

    return StreamProvider<Member?>.value(value: AuthService().user,
    initialData: null,
    child: MaterialApp(home: Wrapper()));

    // return Scaffold(
    //   drawer: SideNavbar(),
    //   appBar: AppBar(
    //       title: Text('EB2App')
    //   ),
    //   body: _screenOptions.elementAt(_selectedIndex),
    //   bottomNavigationBar: BottomNavBar(_changeBody),
    // );
  }
}



